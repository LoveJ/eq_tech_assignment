WS API WORK SAMPLE NODE JS
=====================

A Tech Assignment for EQ written in ReactJS and Node

### Dependencies

* react
* dotenv
* express
* pg
* highcharts
* highcharts-react-official
* react-dom
* react-google-maps
* react-router-dom
* react-scripts
* semantic-ui-react

### dev Dependencies

* babel-plugin-transform-async-to-generator": "6.24.1",
* babel-plugin-transform-es2015-arrow-functions
* babel-preset-es2015
* nodemon
* concurrently // for heroku deployment

## Locally

To run the app server locally run:

```
git clone git@gitlab.com:lovemorejokonya/eq_tech_assignment.git
cd eq_tech_assignment/server
npm install
npm run startdev
```

To run the app client locally run:

```
git clone git@gitlab.com:lovemorejokonya/eq_tech_assignment.git
cd eq_tech_assignment/client
npm install
npm run start
```

or if you use [`yarn`](https://yarnpkg.com):

```
git clone git@gitlab.com:lovemorejokonya/eq_tech_assignment.git
cd eq_tech_assignment
cd server/ cd client
yarn install
yarn run startdev for server or yarn run start for client
```

```
"engines": {
  "node": "8.9.1",
  "yarn": "1.3.2",
  "npm": "5.5.1"
}
```

## Heroku

To deploy on heroku:

```
$ heroku create
$ git push heroku deploy:master
```
make sure its the deploy branch being pushed to heroku because the client and server are combined into one app on the deploy branch for heroku deployment

## Also on Glitch:
[`glitch url`](https://eq-lj-assign.glitch.me/)

currently although both client and server are on glitch and heroku, the client on glitch is pulling data off the server on heroku.
(this is due to a current issue with communication between clientand server on glitch, explained on issues)

# Screen shots

## Screen shot for Home Page
!["Screen shot for Home Page"](https://gitlab.com/LoveJ/eq_tech_assignment/raw/master/app_screen_shots%20/home.png)

## Screenshot for  Table Page
!["Screenshot for  Table Page"](https://gitlab.com/LoveJ/eq_tech_assignment/raw/master/app_screen_shots%20/hourly_events_table.png)

## Screenshot for Charts Page
!["Screenshot for Charts Page"](https://gitlab.com/LoveJ/eq_tech_assignment/raw/master/app_screen_shots%20/hourly_events_chart.png)

## Screenshot for  Map with Impressions
!["Screenshot for  Map with Impressions"](https://gitlab.com/LoveJ/eq_tech_assignment/raw/master/app_screen_shots%20/map_with_impressions.png)

## Screenshot for Map with Clusterer
!["Screenshot for Map with Clusterer"](https://gitlab.com/LoveJ/eq_tech_assignment/raw/master/app_screen_shots%20/map_with_clusterer.png)