import React, { Component } from 'react';
import { Table } from 'semantic-ui-react'
import Pagination from '../utils/Pagination'
// const apiUrl = 'http://localhost:5555/getEventsHourly'
const apiUrl = 'http://localhost:5555/events/hourly'

class Tables extends Component {
  constructor(){
    super()
    this.state = {
      eventsHourly: [],
      pageOfItems: []
    };
  }

  componentDidMount(){
    fetch(apiUrl)
    .then(results =>{
      return results.json();
    }).then(data=>{
      let eventsHourly = data.map((eventHourly,index) => {
        return(
          <Table.Row key={index}>
              <Table.Cell collapsing>
                {eventHourly.date}
              </Table.Cell>
              <Table.Cell collapsing>
                {eventHourly.hour}
              </Table.Cell>
              <Table.Cell collapsing>
                {eventHourly.events}
              </Table.Cell>
            </Table.Row>
        )
      })
      this.setState({eventsHourly: eventsHourly })
    }).catch( err => {
      console.log(err)
    })
  }
  
  onChangePage = (pageOfItems) => {
    // update state with new page of items
    this.setState({ pageOfItems: pageOfItems });
  }
  
  render() {
    return (
      <div>
        <h1>Hourly Events Table</h1>
        <Table  ui="true" celled striped>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Date</Table.HeaderCell>
              <Table.HeaderCell>Hour</Table.HeaderCell>
              <Table.HeaderCell>Events</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {this.state.pageOfItems}
          </Table.Body>
        </Table>
        <Pagination items={this.state.eventsHourly} onChangePage={this.onChangePage} />
      </div>
    );
  }
}

export default Tables;
