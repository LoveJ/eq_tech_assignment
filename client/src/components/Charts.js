import React, { Component } from 'react';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

// allow for exporting chart in different formats
require('highcharts/modules/exporting')(Highcharts)

class Charts extends Component {

  render(props) {
    const data = this.props.hourEventsData
    const events = []
    const hour = []
    const dates = []
    data.map((event)=>{
        events.push(event.events)
        hour.push(event.hour)
        dates.push(event.date)
        return events
    })
    const options = {
        title: {
            text: 'Hourly Events Chart'
        },
        series: [{
            data: events,
            name:  "Events"
          }, {
            data: hour,
            name: "Hour"
          }
        ],
        xAxis: {
            categories: dates,
            crosshair: true,
            title: {
              text: 'DATE'
            }
        }
    }
      

return (
    <div>
        <HighchartsReact
            highcharts={Highcharts}
            options={options}
        />
    </div>
  )
  }
}

export default Charts;
