import React, { Component } from 'react';
import { Divider } from 'semantic-ui-react'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
const { MarkerClusterer } = require('react-google-maps/lib/components/addons/MarkerClusterer')

const onMarkerClustererClick = (markerClusterer: any) => {
    markerClusterer.getMarkers()
}

class Maps extends Component {

  render(props) {
    const data = this.props.poiData
    const poiWIData = this.props.poiWithImpressionsData
    const MyMapComponent = withScriptjs(withGoogleMap((props) =>
        <GoogleMap
            defaultZoom={3}
            defaultCenter={{ lat: 43.6426, lng: -79.3871 }}
        >
        <MarkerClusterer
                onClick={onMarkerClustererClick}
                averageCenter
                enableRetinaIcons
                gridSize={60}
            >
            {data.map((marker, index)=> {
                    return (
                            <Marker
                            key = {index}
                            position={{lat:marker.lat, lng: marker.lon}}
                            title={marker.name}
                            />
                        )
                    })}
                </MarkerClusterer>
            </GoogleMap>
        ))  
            
            const MyPoiWIMapComponent = withScriptjs(withGoogleMap((props) =>
            <GoogleMap
            defaultZoom={8}
            defaultCenter={{ lat: 43.6426, lng: -79.3871 }}
            >
            {poiWIData.map((poimarker, index)=> {
                return (
                    <Marker
                    key = {index}
                    position={{lat:poimarker.lat, lng: poimarker.lon}}
                    title={`
                    Name: ${poimarker.name}
                    Impressions: ${poimarker.impressions}
                            `}>
                    </Marker>
                )
            })}
        </GoogleMap>
    ))  

return (
    <div>
        <Divider horizontal>Map With Impressions</Divider>
        <MyPoiWIMapComponent
            isMarkerShown
            enableRetinaIcons
            googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `400px` }} />}
            mapElement={<div style={{ height: `100%` }} />}
        />
        <Divider horizontal>Map With Clusterer</Divider>
        <MyMapComponent
            isMarkerShown
            googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `400px` }} />}
            mapElement={<div style={{ height: `100%` }} />}
        />
    </div>
  )
  }
}

export default Maps;
