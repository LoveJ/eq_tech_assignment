import React, { Component } from 'react';
import { Header, Icon, Responsive } from 'semantic-ui-react'

class Home extends Component {

  render() {
    return (
      <div>
        <Header as='h1' icon textAlign='center' style={{
        fontSize: '4em',
        fontWeight: 'normal',
        marginBottom: 0,
        marginTop: '3em',
      }}>
          <Icon name='settings' />
          EQ DATA
          <Header.Subheader>Data Visualisations from the EQ Database.</Header.Subheader>
        </Header>
      </div>
    );
  }
}

export default Home;
