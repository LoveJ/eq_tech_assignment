import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import Home from './Home';
import Tables from './Tables';
import Charts from './Charts';
import Maps from './Maps';
// const apiHourEvents = 'http://localhost:5555/getEventsHourly'
// const apiPoi = 'http://localhost:5555/getPoi'
// const apiPoiWithImpressions = 'http://localhost:5555/getPoiWithImpressions'
const apiPoiWithImpressions = 'http://localhost:5555/poiWithImpressions'
const apiHourEvents = 'http://localhost:5555/events/hourly'
const apiPoi = 'http://localhost:5555/poi'

class App extends Component {

  constructor(){
    super()
    this.state = {
      hourEventsData: [],
      poiData: [],
      poiWithImpressionsData: []
    };
  }

  componentDidMount(){
    this.getHourlyEvents(apiHourEvents)
    this.getPoi(apiPoi)
    this.getPoiWithImpressions(apiPoiWithImpressions)
  }
  
  getHourlyEvents = (apiHourEvents)=>{
    fetch(apiHourEvents)
    .then(results =>{
      return results.json();
    }).then(data=>{
        this.setState({hourEventsData: data })
    }).catch( err => {
      console.log(err)
    })
  }

  getPoi = (apiPoi)=>{
    fetch(apiPoi)
    .then(results =>{
      return results.json();
    }).then(data=>{
        this.setState({poiData: data })
    }).catch( err => {
      console.log(err)
    })
  }

  getPoiWithImpressions = (apiPoiWithImpressions)=>{
    fetch(apiPoiWithImpressions)
    .then(results =>{
      return results.json();
    }).then(data=>{
        this.setState({poiWithImpressionsData: data })
    }).catch( err => {
      console.log(err)
    })
  }

  renderHeader() {
    return (
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/tables">Tables</Link>
        </li>
        <li>
          <Link to="/charts">Charts</Link>
        </li>
        <li>
          <Link to="/maps">Maps</Link>
        </li>
      </ul>
    );
  }
  
  render() {
    return (
      <div>
        {this.renderHeader()}
        <Route path="/" exact component={Home} />
        <Route path="/tables" component={Tables} />
        <Route path="/charts" component={() => <Charts hourEventsData={this.state.hourEventsData}/>} />
        <Route path="/maps" component={() => <Maps poiData={this.state.poiData} poiWithImpressionsData={this.state.poiWithImpressionsData}/>} />
      </div>
    );
  }
}

export default App;
