const stats_daily = [
    {
    "date": "2017-01-01T05:00:00.000Z",
    "impressions": "2764609",
    "clicks": "3627",
    "revenue": "13092.1234790000000"
    },
    {
    "date": "2017-01-02T05:00:00.000Z",
    "impressions": "943070",
    "clicks": "1489",
    "revenue": "4275.3479640000000"
    },
    {
    "date": "2017-01-03T05:00:00.000Z",
    "impressions": "962220",
    "clicks": "1274",
    "revenue": "4349.9616000000000"
    },
    {
    "date": "2017-01-04T05:00:00.000Z",
    "impressions": "948574",
    "clicks": "1311",
    "revenue": "4364.3495500000000"
    },
    {
    "date": "2017-01-05T05:00:00.000Z",
    "impressions": "952714",
    "clicks": "1210",
    "revenue": "4496.4799380000000"
    },
    {
    "date": "2017-01-06T05:00:00.000Z",
    "impressions": "1122032",
    "clicks": "1473",
    "revenue": "4733.6558360000000"
    },
    {
    "date": "2017-01-07T05:00:00.000Z",
    "impressions": "1115322",
    "clicks": "1547",
    "revenue": "4644.1067680000000"
    }
]

export default stats_daily