const events_daily = [
    {
    "date": "2017-01-01T05:00:00.000Z",
    "events": "43"
    },
    {
    "date": "2017-01-02T05:00:00.000Z",
    "events": "32"
    },
    {
    "date": "2017-01-03T05:00:00.000Z",
    "events": "11"
    },
    {
    "date": "2017-01-04T05:00:00.000Z",
    "events": "20"
    },
    {
    "date": "2017-01-05T05:00:00.000Z",
    "events": "12"
    },
    {
    "date": "2017-01-06T05:00:00.000Z",
    "events": "42"
    },
    {
    "date": "2017-01-07T05:00:00.000Z",
    "events": "33"
    }
]

export default events_daily