const poi_with_impressions = [
    {
    "date": "2017-01-01T05:00:00.000Z",
    "impressions": "595512",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-01T05:00:00.000Z",
    "impressions": "683110",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-01T05:00:00.000Z",
    "impressions": "868419",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-01T05:00:00.000Z",
    "impressions": "617568",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-02T05:00:00.000Z",
    "impressions": "257515",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-02T05:00:00.000Z",
    "impressions": "282042",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-02T05:00:00.000Z",
    "impressions": "197298",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-02T05:00:00.000Z",
    "impressions": "206215",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-03T05:00:00.000Z",
    "impressions": "325211",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-03T05:00:00.000Z",
    "impressions": "186206",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-03T05:00:00.000Z",
    "impressions": "182399",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-03T05:00:00.000Z",
    "impressions": "268404",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-04T05:00:00.000Z",
    "impressions": "246518",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-04T05:00:00.000Z",
    "impressions": "235897",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-04T05:00:00.000Z",
    "impressions": "155626",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-04T05:00:00.000Z",
    "impressions": "310533",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-05T05:00:00.000Z",
    "impressions": "166856",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-05T05:00:00.000Z",
    "impressions": "324658",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-05T05:00:00.000Z",
    "impressions": "292164",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-05T05:00:00.000Z",
    "impressions": "169036",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-06T05:00:00.000Z",
    "impressions": "310823",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-06T05:00:00.000Z",
    "impressions": "397667",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-06T05:00:00.000Z",
    "impressions": "256935",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-06T05:00:00.000Z",
    "impressions": "156607",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-07T05:00:00.000Z",
    "impressions": "369992",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-07T05:00:00.000Z",
    "impressions": "159984",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-07T05:00:00.000Z",
    "impressions": "163792",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-07T05:00:00.000Z",
    "impressions": "421554",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-08T05:00:00.000Z",
    "impressions": "290050",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-08T05:00:00.000Z",
    "impressions": "288431",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-08T05:00:00.000Z",
    "impressions": "223505",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-08T05:00:00.000Z",
    "impressions": "307102",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-09T05:00:00.000Z",
    "impressions": "202475",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-09T05:00:00.000Z",
    "impressions": "285759",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-09T05:00:00.000Z",
    "impressions": "409976",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-09T05:00:00.000Z",
    "impressions": "215559",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-10T05:00:00.000Z",
    "impressions": "436798",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-10T05:00:00.000Z",
    "impressions": "430199",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-10T05:00:00.000Z",
    "impressions": "120620",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-10T05:00:00.000Z",
    "impressions": "160941",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-11T05:00:00.000Z",
    "impressions": "434756",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-11T05:00:00.000Z",
    "impressions": "358388",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-11T05:00:00.000Z",
    "impressions": "228628",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-11T05:00:00.000Z",
    "impressions": "151043",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-12T05:00:00.000Z",
    "impressions": "186577",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-12T05:00:00.000Z",
    "impressions": "351288",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-12T05:00:00.000Z",
    "impressions": "329746",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-12T05:00:00.000Z",
    "impressions": "285924",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-13T05:00:00.000Z",
    "impressions": "254689",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-13T05:00:00.000Z",
    "impressions": "145318",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-13T05:00:00.000Z",
    "impressions": "279011",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-13T05:00:00.000Z",
    "impressions": "483541",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-14T05:00:00.000Z",
    "impressions": "347102",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-14T05:00:00.000Z",
    "impressions": "149878",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-14T05:00:00.000Z",
    "impressions": "479059",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-14T05:00:00.000Z",
    "impressions": "257582",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-15T05:00:00.000Z",
    "impressions": "79194",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-15T05:00:00.000Z",
    "impressions": "69066",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-15T05:00:00.000Z",
    "impressions": "199288",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-15T05:00:00.000Z",
    "impressions": "184907",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-16T05:00:00.000Z",
    "impressions": "220918",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-16T05:00:00.000Z",
    "impressions": "108862",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-16T05:00:00.000Z",
    "impressions": "340555",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-16T05:00:00.000Z",
    "impressions": "305102",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-17T05:00:00.000Z",
    "impressions": "246630",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-17T05:00:00.000Z",
    "impressions": "295769",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-17T05:00:00.000Z",
    "impressions": "103185",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-17T05:00:00.000Z",
    "impressions": "314750",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-18T05:00:00.000Z",
    "impressions": "276762",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-18T05:00:00.000Z",
    "impressions": "585195",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-18T05:00:00.000Z",
    "impressions": "335974",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-18T05:00:00.000Z",
    "impressions": "163495",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-19T05:00:00.000Z",
    "impressions": "672788",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-19T05:00:00.000Z",
    "impressions": "331529",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-19T05:00:00.000Z",
    "impressions": "333320",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-19T05:00:00.000Z",
    "impressions": "197521",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-20T05:00:00.000Z",
    "impressions": "347996",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-20T05:00:00.000Z",
    "impressions": "303432",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-20T05:00:00.000Z",
    "impressions": "405683",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-20T05:00:00.000Z",
    "impressions": "368748",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-21T05:00:00.000Z",
    "impressions": "230388",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-21T05:00:00.000Z",
    "impressions": "547412",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-21T05:00:00.000Z",
    "impressions": "429967",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-21T05:00:00.000Z",
    "impressions": "148986",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-22T05:00:00.000Z",
    "impressions": "249200",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-22T05:00:00.000Z",
    "impressions": "225755",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-22T05:00:00.000Z",
    "impressions": "332034",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-22T05:00:00.000Z",
    "impressions": "550306",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-23T05:00:00.000Z",
    "impressions": "310697",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-23T05:00:00.000Z",
    "impressions": "451392",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-23T05:00:00.000Z",
    "impressions": "318314",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-23T05:00:00.000Z",
    "impressions": "312402",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-24T05:00:00.000Z",
    "impressions": "546123",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-24T05:00:00.000Z",
    "impressions": "452312",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-24T05:00:00.000Z",
    "impressions": "92355",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-24T05:00:00.000Z",
    "impressions": "283826",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-25T05:00:00.000Z",
    "impressions": "568197",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-25T05:00:00.000Z",
    "impressions": "357624",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-25T05:00:00.000Z",
    "impressions": "260006",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-25T05:00:00.000Z",
    "impressions": "180410",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-26T05:00:00.000Z",
    "impressions": "379472",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-26T05:00:00.000Z",
    "impressions": "294992",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-26T05:00:00.000Z",
    "impressions": "228431",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-26T05:00:00.000Z",
    "impressions": "424762",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-27T05:00:00.000Z",
    "impressions": "490149",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-27T05:00:00.000Z",
    "impressions": "276937",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-27T05:00:00.000Z",
    "impressions": "216189",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-27T05:00:00.000Z",
    "impressions": "321458",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-28T05:00:00.000Z",
    "impressions": "356857",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-28T05:00:00.000Z",
    "impressions": "352689",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-28T05:00:00.000Z",
    "impressions": "202601",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-28T05:00:00.000Z",
    "impressions": "558559",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-29T05:00:00.000Z",
    "impressions": "430247",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-29T05:00:00.000Z",
    "impressions": "243464",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-29T05:00:00.000Z",
    "impressions": "278601",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-29T05:00:00.000Z",
    "impressions": "497050",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-30T05:00:00.000Z",
    "impressions": "351313",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-30T05:00:00.000Z",
    "impressions": "425944",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-30T05:00:00.000Z",
    "impressions": "268539",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-30T05:00:00.000Z",
    "impressions": "371156",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-01-31T05:00:00.000Z",
    "impressions": "507882",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-01-31T05:00:00.000Z",
    "impressions": "240778",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-01-31T05:00:00.000Z",
    "impressions": "334548",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-01-31T05:00:00.000Z",
    "impressions": "448965",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-02-01T05:00:00.000Z",
    "impressions": "329713",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-02-01T05:00:00.000Z",
    "impressions": "121565",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-02-01T05:00:00.000Z",
    "impressions": "592420",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-02-01T05:00:00.000Z",
    "impressions": "448011",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-02-02T05:00:00.000Z",
    "impressions": "315339",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-02-02T05:00:00.000Z",
    "impressions": "247287",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-02-02T05:00:00.000Z",
    "impressions": "316134",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-02-02T05:00:00.000Z",
    "impressions": "712486",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-02-03T05:00:00.000Z",
    "impressions": "355620",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-02-03T05:00:00.000Z",
    "impressions": "588270",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-02-03T05:00:00.000Z",
    "impressions": "343075",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-02-03T05:00:00.000Z",
    "impressions": "412991",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-02-04T05:00:00.000Z",
    "impressions": "200077",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-02-04T05:00:00.000Z",
    "impressions": "656508",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-02-04T05:00:00.000Z",
    "impressions": "65635",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-02-04T05:00:00.000Z",
    "impressions": "834630",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-02-05T05:00:00.000Z",
    "impressions": "481232",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-02-05T05:00:00.000Z",
    "impressions": "103095",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-02-05T05:00:00.000Z",
    "impressions": "381483",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-02-05T05:00:00.000Z",
    "impressions": "818969",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-02-06T05:00:00.000Z",
    "impressions": "498195",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-02-06T05:00:00.000Z",
    "impressions": "411205",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-02-06T05:00:00.000Z",
    "impressions": "296214",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-02-06T05:00:00.000Z",
    "impressions": "475250",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-02-07T05:00:00.000Z",
    "impressions": "170185",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-02-07T05:00:00.000Z",
    "impressions": "473931",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-02-07T05:00:00.000Z",
    "impressions": "227868",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-02-07T05:00:00.000Z",
    "impressions": "290412",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-02-08T05:00:00.000Z",
    "impressions": "545206",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-02-08T05:00:00.000Z",
    "impressions": "540764",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-02-08T05:00:00.000Z",
    "impressions": "497472",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-02-08T05:00:00.000Z",
    "impressions": "339529",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-02-09T05:00:00.000Z",
    "impressions": "381584",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-02-09T05:00:00.000Z",
    "impressions": "431102",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-02-09T05:00:00.000Z",
    "impressions": "408438",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-02-09T05:00:00.000Z",
    "impressions": "500841",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-02-10T05:00:00.000Z",
    "impressions": "125210",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-02-10T05:00:00.000Z",
    "impressions": "569097",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-02-10T05:00:00.000Z",
    "impressions": "824304",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-02-10T05:00:00.000Z",
    "impressions": "396317",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    },
    {
    "date": "2017-02-11T05:00:00.000Z",
    "impressions": "392853",
    "lat": 43.0896,
    "lon": -79.0849,
    "name": "Niagara Falls"
    },
    {
    "date": "2017-02-11T05:00:00.000Z",
    "impressions": "481959",
    "lat": 43.6708,
    "lon": -79.3899,
    "name": "EQ Works"
    },
    {
    "date": "2017-02-11T05:00:00.000Z",
    "impressions": "508595",
    "lat": 43.6426,
    "lon": -79.3871,
    "name": "CN Tower"
    },
    {
    "date": "2017-02-11T05:00:00.000Z",
    "impressions": "412664",
    "lat": 49.2965,
    "lon": -123.0884,
    "name": "Vancouver Harbour"
    }
]

export default poi_with_impressions