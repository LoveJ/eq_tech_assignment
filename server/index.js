const express = require('express')
const pg = require('pg')
require('dotenv').config()
const app = express()
const rateLimit = require('./utils/rateLimit.js').default

app
  .use(rateLimit)
  // .use(cors());
  // alternatively you could use this function without importing cors
  .use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    // res.body("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

// configs come from standard PostgreSQL env vars
// https://www.postgresql.org/docs/9.6/static/libpq-envars.html
const pool = new pg.Pool()

//import temporary fake data for offline testing
const events_daily  = require ('./data/events_daily.js').default
const events_hourly  = require ('./data/events_hourly.js').default
const poi  = require ('./data/poi.js').default
const stats_daily  = require ('./data/stats_daily.js').default
const stats_hourly  = require ('./data/stats_hourly.js').default
const poi_with_impressions = require ('./data/poi_with_impressions.js').default
//end of fake data imports

const queryHandler = (req, res, next) => {
  pool.query(req.sqlQuery).then((r) => {
    return res.json(r.rows || [])
  }).catch(next)
}

app.get('/', (req, res) => {
  res.send('Welcome to EQ Works 😎')
})

app.get('/events/hourly', (req, res, next) => {
  req.sqlQuery = `
    SELECT date, hour, events
    FROM public.hourly_events
    ORDER BY date, hour
    LIMIT 168;
  `
  return next()
}, queryHandler)

app.get('/events/daily', (req, res, next) => {
  req.sqlQuery = `
    SELECT date, SUM(events) AS events
    FROM public.hourly_events
    GROUP BY date
    ORDER BY date
    LIMIT 7;
  `
  return next()
}, queryHandler)

app.get('/stats/hourly', (req, res, next) => {
  req.sqlQuery = `
    SELECT date, hour, impressions, clicks, revenue
    FROM public.hourly_stats
    ORDER BY date, hour
    LIMIT 168;
  `
  return next()
}, queryHandler)

app.get('/stats/daily', (req, res, next) => {
  req.sqlQuery = `
    SELECT date,
        SUM(impressions) AS impressions,
        SUM(clicks) AS clicks,
        SUM(revenue) AS revenue
    FROM public.hourly_stats
    GROUP BY date
    ORDER BY date
    LIMIT 7;
  `
  return next()
}, queryHandler)

app.get('/poi', (req, res, next) => {
  req.sqlQuery = `
    SELECT *
    FROM public.poi;
  `
  return next()
}, queryHandler)

app.get('/poiWithImpressions', (req, res, next) => {
  const { table, column } = req.query
  req.sqlQuery = `
    SELECT public.hourly_stats.date AS date, SUM(public.hourly_stats.impressions) AS impressions, public.poi.lat AS lat, public.poi.lon AS lon, public.poi.name AS name
        FROM public.poi, public.hourly_stats
        WHERE public.hourly_stats.poi_id = public.poi.poi_id
        GROUP BY date, name, lat, lon
        ORDER BY date
        LIMIT 168;
      `
  return next()
}, queryHandler)

//temporary fake routes for offline testing
app.get('/getEventsDaily', function(req, res, next){
  res.json(events_daily);
})

app.get('/getEventsHourly', function(req, res, next){
  res.json(events_hourly);
})

app.get('/getStatsDaily', function(req, res, next){
  res.json(stats_daily);
})

app.get('/getStatsHourly', function(req, res, next){
  res.json(stats_hourly);
})

app.get('/getPoi', function(req, res, next){
  res.json(poi);
})

app.get('/getPoiWithImpressions', function(req, res, next){
  res.json(poi_with_impressions);
})

//END OF FAKE ROUTES FOR OFFLINE TESTING

app.listen(process.env.PORT || 5555, (err) => {
  if (err) {
    console.error(err)
    process.exit(1)
  } else {
    console.log(`Running on ${process.env.PORT || 5555}`)
  }
})

// last resorts
process.on('uncaughtException', (err) => {
  console.log(`Caught exception: ${err}`)
  process.exit(1)
})
process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason)
  process.exit(1)
})
