var recentRequestTimes = [];
var maxRequests = 50;
var maxRequestsTime = 1000;

const rateLimit = (req,res,next) => {
    // oldest request time is at front of the array
    if (req){
      console.log('rateLimit works')
      var old, now = Date.now();
      recentRequestTimes.push(now);
      if (recentRequestTimes.length >= maxRequests) {
      // get the oldest request time and examine it
      old = recentRequestTimes.shift();
          if (now - old <= maxRequestsTime) {
              // old request was not very long ago, too many coming in during that maxRequestsTime
              console.log('Exceeded 50 requests per second for events')
              res.status(503).json({error: 'Exceeded 50 requests per second for events'});
              return;
          }
        }
    }
    next();
  }

export default rateLimit